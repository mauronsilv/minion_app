class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

#  def create
#    @user = User.new(user_params)
#    
#    if @user.save
#      flash[:success] = "Purchase was successful!"       
#      redirect_to @user
#    else
#      render 'new'
#    end
#  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        flash[:success] = "Purchase was successful!"
        #redirect_to @user

        # Sends email to user when user is created.
        ExampleMailer.sample_email(@user).deliver

        format.html { redirect_to @user, notice: 'Purchase was successful!' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :shipping_address, 
                                   :billing_address, :card_number, 
                                   :card_expiration_date, :card_security_code, 
                                   :giftwrap)
    end
end

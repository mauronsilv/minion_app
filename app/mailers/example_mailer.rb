class ExampleMailer < ActionMailer::Base
  default from: "minionincCEO@gmail.com"

  def sample_email(user)
    @user = user
    @url  = 'http://www.gmail.com'
    mail(to: "miguel@inventosdigitais.com.br", subject: 'New Purchase Made')
  end
end

class User
  attr_accessor :name, :email, :shipping_address, :billing_address, :card_number, :card_expiration_date, :card_security_code, :giftwrap

  def initialize(attributes = {})
    @name  = attributes[:name]
    @email = attributes[:email]
    @shipping_address = attributes[:shipping_address]
    @billing_address = attributes[:billing_address]
    @card_number = attributes[:card_number]
    @card_expiration_date = attributes[:card_expiration_date]
    @card_security_code = attributes[:card_security_code]
    @giftwrap = attributes[:giftwrap]
  end

  def formatted_email
    "#{@name} <#{@email}>"
  end
end

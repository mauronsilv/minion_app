class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :shipping_address
      t.string :billing_address
      t.string :card_number
      t.string :card_expiration_date
      t.string :card_security_code
      t.boolean :giftwrap

      t.timestamps
    end
  end
end
